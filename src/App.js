import "./App.css";
import { useState } from "react";
import { Card } from "antd";
import "antd/dist/antd.css";

function App() {
  const [user, setUser] = useState({});
  const [active, setActive] = useState(false);

  const handleToggle = () => {
    fetch("https://api.github.com/users/claudio-42")
      .then((res) => res.json())
      .then((user) => {
        console.log(user);
        setUser({ ...user });
        setActive(!active);
      });
  };
  return (
    <div className="App">
      <header className="App-header">
        {active && (
          <Card>
            <img src={user.avatar_url} />

            <ul className="info">
              <li>{user.login}</li>
              <li>{user.bio}</li>
              <li>{user.location}</li>
            </ul>
          </Card>
        )}
        <button className="botao" onClick={handleToggle}>
          Click
        </button>
      </header>
    </div>
  );
}

export default App;
